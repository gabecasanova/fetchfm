import express from "express";
import cors from "cors";
import { supabase } from "./dbConfig.js";

const app = express();

const CLIENT_PORT = process.env.CLIENT_PORT || 3000;
const SERVER_PORT = process.env.SERVER_PORT || 5000;

var corsOptions = {
  origin: `http://localhost:${CLIENT_PORT}`,
  optionsSuccessStatus: 200,
};

// Apply CORS middleware globally
app.use(cors(corsOptions));

// Middleware to parse JSON bodies
app.use(express.json());

app.get("/", (req, res) => {
  res.send("Hello, I am working my friend!");
});

app.listen(SERVER_PORT, () =>
  console.log(`Server is running on port ${SERVER_PORT}`)
);
