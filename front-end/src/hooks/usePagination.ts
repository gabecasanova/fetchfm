import { useMemo } from "react";

interface Props {
  totalCount: number;
  currentPage: number;
  pageSize: number;
  siblingCount?: number;
}

/**
 * Custom `usePagination` hook to generate an array representing the pagination structure.
 *
 * @param {number} totalCount - The total count of items available for pagination.
 * @param {number} currentPage - The current active page, starting at 1.
 * @param {number} pageSize - The number of items displayed per page.
 * @param {number} [siblingCount=1] - The number of pagination buttons displayed on each side of the current page button. Defaults to `1`.
 *
 * @returns {(string | number)[]} An array representing the pagination structure. The array contains page numbers, and if applicable, `"..."` to indicate skipped page ranges.
 *
 * @example
 * // Example usage:  [1, '...', 4, 5, 6, '...', 10]
 * const paginationRange = usePagination({
 *   totalCount: 100,
 *   currentPage: 5,
 *   pageSize: 10,
 *   siblingCount: 1
 * });
 */
const usePagination = ({
  totalCount,
  currentPage,
  pageSize,
  siblingCount = 1,
}: Props): (string | number)[] => {
  const range = (start: number, end: number) => {
    let length = end - start + 1;
    /*
      Create an array of certain length and set the elements within it from
      start value to end value.
    */
    return Array.from({ length }, (_, idx) => idx + start);
  };

  const paginationRange = useMemo(() => {
    const totalPageCount = Math.ceil(totalCount / pageSize);

    // `totalPagePills` is calculated as siblingCount + firstPage + lastPage + currentPage + 2*DOTS
    const totalPagePills = siblingCount + 5;

    /*
      CASE 1: If the total page count is less than the number of pills in our component, return the range [1..totalPageCount]
    */
    if (totalPageCount <= totalPagePills) {
      return range(1, totalPageCount);
    }

    // Calculate currentPage's left and right sibling indexes, ensuring they're within upper/lower limits (i.e 1 and totalPageCount)
    const leftSiblingIndex = Math.max(currentPage - siblingCount, 1);
    const rightSiblingIndex = Math.min(
      currentPage + siblingCount,
      totalPageCount
    );

    // We don't render DOTS when there's just one page pill b/w a sibling pill and the upper/lower limits
    const shouldShowLeftDots = leftSiblingIndex > 2;
    const shouldShowRightDots = rightSiblingIndex < totalPageCount - 1;

    const firstPageIndex = 1;
    const lastPageIndex = totalPageCount;

    /*
      CASE 2: No left dots to show, but rights dots to be shown
    */
    if (!shouldShowLeftDots && shouldShowRightDots) {
      let leftItemCount = 2 + 2 * siblingCount;
      let leftRange = range(1, leftItemCount);
      return [...leftRange, "...", totalPageCount];
    }

    /*
      Case 3: No right dots to show, but left dots to be shown
    */
    if (shouldShowLeftDots && !shouldShowRightDots) {
      let rightItemCount = 2 + 2 * siblingCount;
      let rightRange = range(
        totalPageCount - rightItemCount + 1,
        totalPageCount
      );
      return [firstPageIndex, "...", ...rightRange];
    }

    /*
      Case 4: Both left and right dots to be shown
    */
    if (shouldShowLeftDots && shouldShowRightDots) {
      let middleRange = range(leftSiblingIndex, rightSiblingIndex);
      return [firstPageIndex, "...", ...middleRange, "...", lastPageIndex];
    }

    return [];
  }, [totalCount, pageSize, siblingCount, currentPage]);

  return paginationRange;
};

export default usePagination;

// https://www.freecodecamp.org/news/build-a-custom-pagination-component-in-react/
