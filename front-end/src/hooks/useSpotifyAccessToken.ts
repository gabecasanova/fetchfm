import { useEffect, useState } from "react";
import { getSpotifyAccessToken, getStoredToken } from "api/spotify/spotifyApi";
import { isTimestampInPast } from "utils/date";

const useSpotifyAccessToken = () => {
  const [accessToken, setAccessToken] = useState<string | null>(null);

  // define function to fetch new Spotify Access Token
  const fetchNewAccessToken = async () => {
    const response = await getSpotifyAccessToken(); // also saves it to localStorage
    if (response) {
      setAccessToken(response.access_token);
    }
  };

  /**
   * On initial load, check `localStorage` for an existing token:
   *  1. If token exists and hasn't expired, use it by setting `accessToken`
   *  2. Otherwise, fetch a new token
   *
   * Then, run `setInterval` to automatically fetch a new access token every 59 minutes.
   */
  useEffect(() => {
    // retrieve stored Spotify Access Token from `localStorage`
    const storedToken = getStoredToken();

    if (!storedToken || isTimestampInPast(storedToken.expiresAt)) {
      fetchNewAccessToken();
    } else {
      setAccessToken(storedToken.token);
    }

    // Automatically fetch new access token every 59 minutes (i.e. 1 min before expiration)
    const refreshTimer = setInterval(() => {
      fetchNewAccessToken();
    }, 59 * 60 * 1000);

    return () => clearInterval(refreshTimer);
  }, []); // on initial render only

  return accessToken;
};

export default useSpotifyAccessToken;
