import { useEffect, useMemo, useRef } from "react";
import { debounce } from "lodash";

interface Props {
  callback: () => void;
  delay: number;
}

const useDebounce = ({ callback, delay }: Props) => {
  // create ref and initialize it with the callback function
  const ref = useRef(callback);

  useEffect(() => {
    // updating ref when state changes
    // now, ref.current will have the latest callback with access to the latest state
    ref.current = callback;
  }, [callback]);

  // creating debounced callback only once - on mount
  const debouncedCallback = useMemo(() => {
    // func will be created only once - on mount
    const func = () => {
      // ref is mutable! ref.current is a reference to the latest callback
      ref.current?.();
    };
    // debounce the func that was created once, but has access to the latest callback
    return debounce(func, delay);
  }, [delay]);

  return debouncedCallback;
};

export default useDebounce;

// https://www.developerway.com/posts/debouncing-in-react
