import { BrowserRouter, Route, Routes } from "react-router-dom";
import { SpotifyTokenProvider } from "context/SpotifyTokenContext";
import Login from "pages/Login";
import Profile from "pages/Profile";
import "./App.css";

function App() {
  return (
    <SpotifyTokenProvider>
      <BrowserRouter>
        <Routes>
          <Route path="/*" element={<Login />} />
          <Route path="/user/:username" element={<Profile />} />
        </Routes>
      </BrowserRouter>
    </SpotifyTokenProvider>
  );
}

export default App;
