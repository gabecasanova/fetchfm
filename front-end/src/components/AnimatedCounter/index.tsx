import { useRef } from "react";
import { animate, useInView, useIsomorphicLayoutEffect } from "framer-motion";
import { formatWithCommas } from "utils/numbers";

interface Props {
  from: number;
  to: number;
  duration?: number;
  includeCommas?: boolean;
  animateOnce?: boolean;
}

/**
 * `AnimatedCounter` is a React component that animates a number between a starting and
 * ending value, while also detecting and respecting the user's `reduced-motion` preferences.
 *
 * @param from - The starting value of the counter.
 * @param to - The ending value of the counter.
 * @param duration - The duration of the animation in seconds. Defaults to `1.25`.
 * @param includeCommas - Whether to format the number with commas. Defaults to `true`.
 * @param animateOnce - Whether to animate only once when the counter enters view. Defaults to `true`.
 * @returns A `<span>` displaying the animated number.
 */
const AnimatedCounter = ({
  from,
  to,
  duration = 1.25,
  includeCommas = true,
  animateOnce = true,
}: Props) => {
  const ref = useRef<HTMLSpanElement | null>(null);
  const inView = useInView(ref, { once: animateOnce });

  useIsomorphicLayoutEffect(() => {
    const element = ref.current;

    if (!element || !inView) return;

    const getFormattedValue = (value: number) =>
      includeCommas ? formatWithCommas(value.toFixed(0)) : value.toFixed(0);

    if (window.matchMedia("(prefers-reduced-motion)").matches) {
      element.textContent = getFormattedValue(to);
      return;
    }

    element.textContent = getFormattedValue(from);
    
    const controls = animate(from, to, {
      duration,
      ease: "easeOut",
      onUpdate(val) {
        element.textContent = getFormattedValue(val);
      },
    });

    return () => {
      controls.stop();
    };
  }, [ref, inView, from, to]);

  return <span ref={ref} />;
};

export default AnimatedCounter;
