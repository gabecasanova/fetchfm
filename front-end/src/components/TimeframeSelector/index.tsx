import { useState } from "react";
import Select, { components, SingleValueProps } from "react-select";
import { FaAngleDown, FaAngleUp } from "react-icons/fa";
import "./TimeframeSelector.css";

/**
 * Valid timeframes to select from.
 */
export const timeframes = {
  ALL_TIME: "All time",
  LAST_7_DAYS: "Last 7 days",
  LAST_30_DAYS: "Last 30 days",
  LAST_90_DAYS: "Last 90 days",
  LAST_180_DAYS: "Last 180 days",
  LAST_365_DAYS: "Last 365 days",
};

// Dynamically generates 'options': [{value: string, label: string}]
const options = Object.values(timeframes).map((val) => ({
  value: val.toLowerCase(),
  label: val,
}));

// Component props for the 'TimeframeSelector'
interface Props {
  selectedTimeframe: string;
  setSelectedTimeframe: React.Dispatch<React.SetStateAction<string>>;
}

const TimeframeSelector = ({
  selectedTimeframe,
  setSelectedTimeframe,
}: Props) => {
  const [isFocused, setIsFocused] = useState(false);

  // Find the 'option' that matches `selectedTimeframe`
  const selectedOption = options.find(
    (option) => option.label === selectedTimeframe
  );

  // Renders an 'Arrow' icon within the 'selected value' portion of the dropdown box.
  const CustomSingleValue = (
    props: SingleValueProps<{ value: string; label: string }>
  ) => {
    const ArrowIcon = isFocused ? FaAngleUp : FaAngleDown;
    return (
      <components.SingleValue {...props}>
        <div style={{ display: "flex", alignItems: "center", gap: "6px" }}>
          {props.data.label}
          <ArrowIcon style={{ marginLeft: -2, paddingTop: 3.5 }} />
        </div>
      </components.SingleValue>
    );
  };

  return (
    <div style={{ position: "relative" }}>
      <div className="timeframe-selector">
        <Select
          value={selectedOption}
          onChange={(option) =>
            setSelectedTimeframe(option ? option.label : "")
          }
          options={options}
          isMulti={false}
          isSearchable={false}
          styles={customStyles}
          components={{
            SingleValue: CustomSingleValue,
            IndicatorSeparator: () => null,
            DropdownIndicator: () => null,
          }}
          onMenuOpen={() => setIsFocused(true)}
          onMenuClose={() => setIsFocused(false)}
          theme={(theme) => ({
            ...theme,
            colors: {
              ...theme.colors,
              primary: "white",
              primary50: "white",
            },
          })}
        />
      </div>
    </div>
  );
};

const customStyles = {
  control: (defaultStyles: any) => ({
    // the dropdown box
    ...defaultStyles,
    border: "none",
    boxShadow: "none",
    backgroundColor: "transparent",
  }),
  singleValue: (defaultStyles: any) => ({
    // the 'text and icon' within the dropdown box
    ...defaultStyles,
    cursor: "pointer",
    color: "black",
    fontSize: "larger",
    margin: 0,
    paddingLeft: 2.5,
    fontWeight: 700,
  }),
  menu: (defaultStyles: any) => ({
    // the menu containing dropdown items
    ...defaultStyles,
    textAlign: "right",
  }),
  option: (defaultStyles: any) => ({
    // items within the dropdown menu
    ...defaultStyles,
    color: "black",
    backgroundColor: "white",
    "&:hover": {
      color: "white",
      backgroundColor: "black",
    },
    cursor: "pointer",
    fontWeight: 600,
  }),
};

export default TimeframeSelector;
