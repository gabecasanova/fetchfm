import { MdKeyboardArrowLeft, MdKeyboardArrowRight } from "react-icons/md";
import usePagination from "hooks/usePagination";
import useScreenWidth from "hooks/useScreenWidth";
import "./ListPager.css";

interface Props {
  totalCount: number;
  currentPage: number;
  pageSize: number;
  setCurrentPage: React.Dispatch<React.SetStateAction<number>>;
  siblingCount?: number;
}

/**
 * `ListPager` is a React component that provides pagination controls with navigation arrows and page numbers.
 *
 * @param totalCount - The total number of items to paginate through.
 * @param currentPage - The current active page.
 * @param pageSize - The number of items per page.
 * @param setCurrentPage - Function to update the current page.
 * @param {number} [siblingCount=1] - Number of sibling pages to show on each side of the current page. Defaults to `1`.
 *
 * @returns {JSX.Element | null} Pagination controls, or `null` if there are fewer than 2 pages.
 */
const ListPager = ({
  totalCount,
  currentPage,
  pageSize,
  setCurrentPage,
  siblingCount = 1,
}: Props) => {
  let screenWidth = useScreenWidth();
  const compactSiblingCount = screenWidth <= 700 ? 1 : siblingCount;

  const paginationRange = usePagination({
    totalCount,
    currentPage,
    pageSize,
    siblingCount: compactSiblingCount,
  })!;

  // Don't render component when there's fewer than two pages
  if (currentPage === 0 || paginationRange.length < 2) {
    return null;
  }

  const onNext = () => {
    setCurrentPage(currentPage + 1);
  };

  const onPrevious = () => {
    setCurrentPage(currentPage - 1);
  };

  const lastPage = paginationRange[paginationRange.length - 1];

  return (
    <ul className="list-pager">
      {/* Left Nav Arrow */}
      <li
        style={{ display: `${currentPage === 1 ? "none" : "block"}` }}
        onClick={onPrevious}
      >
        <div className="list-pager__nav-arrow left">
          <MdKeyboardArrowLeft size={20} />
        </div>
      </li>
      {/* Pill Container */}
      <ul className="list-pager__pill-container">
        {paginationRange.map((pagePill, index) => {
          if (pagePill === "...") {
            return (
              <li className="list-pager__pill dots" key={index}>
                &#8230;
              </li>
            );
          } else {
            return (
              <li
                className={`list-pager__pill ${
                  pagePill === currentPage ? "selected" : ""
                }`}
                key={index}
                onClick={() => setCurrentPage(Number(pagePill))}
              >
                {pagePill}
              </li>
            );
          }
        })}
      </ul>
      {/*  Right Nav Arrow */}
      <li
        style={{ display: `${currentPage === lastPage ? "none" : "block"}` }}
        onClick={onNext}
      >
        <div className="list-pager__nav-arrow right">
          <MdKeyboardArrowRight size={20} />
        </div>
      </li>
    </ul>
  );
};

export default ListPager;

// https://www.freecodecamp.org/news/build-a-custom-pagination-component-in-react/
