import React, { createContext, useContext, useEffect, useState } from "react";
import useSpotifyAccessToken from "hooks/useSpotifyAccessToken";

interface SpotifyTokenContextType {
  accessToken: string | null;
}

const SpotifyTokenContext = createContext<SpotifyTokenContextType | null>(null);

// Custom hook for accessing the context
export const useSpotifyTokenContext = (): SpotifyTokenContextType => {
  const context = useContext(SpotifyTokenContext);
  if (!context) {
    throw new Error(
      "useSpotifyTokenContext() must be used within a SpotifyTokenProvider"
    );
  }
  return context;
};

// Provider component
export const SpotifyTokenProvider: React.FC<{ children: React.ReactNode }> = ({
  children,
}) => {
  const [accessToken, setAccessToken] = useState<string | null>(null);

  const token = useSpotifyAccessToken();
  useEffect(() => {
    if (token) {
      setAccessToken(token); // update context state whenever the token changes
    }
  }, [token]);

  return (
    <SpotifyTokenContext.Provider value={{ accessToken }}>
      {children}
    </SpotifyTokenContext.Provider>
  );
};
