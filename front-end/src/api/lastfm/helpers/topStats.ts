import { LastfmTrack } from "api/lastfm/interfaces";
import { getSpotifyCatalogInfo } from "api/spotify/spotifyApi";
import { defaultLastfmImages } from "assets/images";
import { getLastfmArtistUrl, getLastfmAlbumUrl } from "./lastfmUrls";

/**
 * Represents the most frequent `artist`, `album`, or `song`:
 * - `type` - "Artist" | "Album" | "Song"
 * - `name` - item name
 * - `image` - image url
 * - `url` - lastfm url
 * - `count` - scrobble count
 */
export interface LastfmTopItem {
  type: "Artist" | "Album" | "Song";
  name: string;
  image: string;
  url: string;
  count: number;
}

/**
 * Represents the user's most listened-to `artist`, `album`, and `song` from a collection of tracks.
 *
 * `TopStats`:
 * - `artist: LastfmTopItem`
 * - `album: LastfmTopItem`
 * - `song: LastfmTopItem`
 *
 * `LastfmTopItem`: `{ type, name, image, url, count }`
 */
export interface LastfmTopStats {
  artist: LastfmTopItem;
  album: LastfmTopItem;
  song: LastfmTopItem;
}

/**
 * Gets the user's most listened-to `artist`, `album`, and `song` from a non-empty list of tracks.
 *
 * @param tracks - The array of LastfmTrack objects to search.
 * @returns An object containing {`artist`, `album`, `song`}.
 * @throws {Error} If the input `tracks` array is empty.
 */
export async function getTopStats(
  tracks: LastfmTrack[]
): Promise<LastfmTopStats> {
  const artist = await getTopArtist(tracks!);
  const album = getTopAlbum(tracks!);
  const song = getTopSong(tracks!);
  return { artist, album, song };
}

/**
 * Gets the most frequent Last.fm artist from a non-empty list of tracks.
 *
 * @remarks
 * This function calls Spotify's API to retrieve the artist's image url.
 *
 * @param tracks - The array of LastfmTrack objects to search.
 * @returns An object containing {`type`, `name`, `image`, `url`, `count`}.
 * @throws {Error} If the input `tracks` array is empty.
 */
export async function getTopArtist(
  tracks: LastfmTrack[]
): Promise<LastfmTopItem> {
  const result = getTopItem("artist", tracks);
  const artistName = result.track.artist["#text"];

  // Use Spotify's API to obtain artist's image b/c Last.fm API no longer provides it
  const response = await getSpotifyCatalogInfo(artistName, "artist");

  const artist = response?.artists?.items?.[0]; // SpotifyArtist | undefined
  const image = artist?.images?.[0]?.url || defaultLastfmImages.artist;

  return {
    type: "Artist",
    name: result.track.artist["#text"],
    image,
    url: getLastfmArtistUrl(result.track.url),
    count: result.count,
  };
}

/**
 * Gets the most frequent Last.fm album from a non-empty list of tracks.
 * @param tracks - The array of LastfmTrack objects to search.
 * @returns An object containing {`type`, `name`, `image`, `url`, `count`}.
 * @throws {Error} If the input `tracks` array is empty.
 */
export function getTopAlbum(tracks: LastfmTrack[]): LastfmTopItem {
  const result = getTopItem("album", tracks);
  return {
    type: "Album",
    name: result.track.album["#text"],
    image: result.track.image[3]["#text"] || defaultLastfmImages.album,
    url: getLastfmAlbumUrl(result.track.album["#text"], result.track.url),
    count: result.count,
  };
}

/**
 * Gets the most frequent Last.fm song from a non-empty list of tracks.
 * @param tracks - The array of LastfmTrack objects to search.
 * @returns An object containing {`type`, `name`, `image`, `url`, `count`}.
 * @throws {Error} If the input `tracks` array is empty.
 */
export function getTopSong(tracks: LastfmTrack[]): LastfmTopItem {
  const result = getTopItem("song", tracks);
  return {
    type: "Song",
    name: result.track.name,
    image: result.track.image[3]["#text"] || defaultLastfmImages.track,
    url: result.track.url,
    count: result.count,
  };
}

/**
 * Finds the most frequent `LastfmTrack` of the specified `type` from a list of tracks.
 * @param type - "artist" | "album" | "song".
 * @param tracks - The array of LastfmTrack objects to search.
 * @returns An object containing {`track`, `count`}.
 * @throws {Error} If the input `tracks` array is empty.
 */
function getTopItem(
  type: "artist" | "album" | "song",
  tracks: LastfmTrack[]
): {
  track: LastfmTrack;
  count: number;
} {
  if (tracks.length === 0) {
    throw new Error(`Cannot find top ${type}: No tracks provided.`);
  }

  const freqs: Record<string, number> = {};
  let maxFrequency = 0;
  let topTrack: LastfmTrack | null = null;

  // determine which property to use based on `type` passed in
  let getQuery: (track: LastfmTrack) => string;

  switch (type) {
    case "artist":
      getQuery = (track) => track.artist["#text"];
      break;
    case "album":
      getQuery = (track) => track.album["#text"];
      break;
    case "song":
      getQuery = (track) => track.name;
      break;
    default:
      break;
  }

  tracks.forEach((track) => {
    const query = getQuery(track);

    if (query) {
      freqs[query] = (freqs[query] || 0) + 1;

      if (freqs[query] > maxFrequency) {
        maxFrequency = freqs[query];
        topTrack = track;
      }
    }
  });

  if (!topTrack) {
    throw new Error(
      `Cannot find top ${type}: No valid ${type} property found.`
    );
  }

  return { track: topTrack, count: maxFrequency };
}
