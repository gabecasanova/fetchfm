/**
 * Generates a Last.fm artist URL from a given song URL.
 * @param songUrl - The Last.fm song URL associated with the artist.
 * @returns The corresponding Last.fm artist URL.
 */
export function getLastfmArtistUrl(songUrl: string): string {
  const res = songUrl.split("/");
  return `https://www.last.fm/music/${res[4]}`;
}

/**
 * Generates a Last.fm album URL from a given album name and song URL.
 * @param albumName - The name of the album.
 * @param songUrl - The Last.fm song URL associated with the artist.
 * @returns The corresponding Last.fm album URL.
 */
export function getLastfmAlbumUrl(albumName: string, songUrl: string): string {
  const encodedAlbumName = encodeURIComponent(albumName)
    .replaceAll("%20", "+")
    .replaceAll("%2C", ",");
  return getLastfmArtistUrl(songUrl) + "/" + encodedAlbumName;
}

/**
 * Generates the Last.fm URL linking to a user's scrobbles for a specific date.
 * @param username - The Last.fm username.
 * @param dayOfTheYear - The day of the year (1-365, or 1-366 for leap years).
 * @param year - The year of the desired date.
 * @returns The Last.fm URL pointing to the user's scrobbles for the given date.
 */
export function getLastfmScrobblesUrlForDate(
  username: string,
  dayOfTheYear: number,
  year: number
): string {
  let d = new Date(year, 0, dayOfTheYear);
  let formattedDate = new Date(d.getTime() - d.getTimezoneOffset() * 60_000)
    .toISOString()
    .split("T")[0];
  return `https://www.last.fm/user/${username}/library?from=${formattedDate}&rangetype=1day`;
}

/**
 * Generates the Last.fm URL linking to a user's library.
 *
 * @param username - The Last.fm username.
 * @param {string} [path=""] - An optional path to append to the library URL (e.g., "/artists" or "/albums").
 * @returns The complete Last.fm library URL.
 *
 * @example
 * // Returns "https://www.last.fm/user/johndoe/library"
 * getLastfmLibraryUrl("johndoe");
 *
 * @example
 * // Returns "https://www.last.fm/user/johndoe/library/albums"
 * getLastfmLibraryUrl("johndoe", "/albums");
 */
export function getLastfmLibraryUrl(username: string, path = "") {
  return `https://www.last.fm/user/${username}/library${path}`;
}
