import axios from "axios";
import { LastfmResponse } from "./interfaces";

/**
 * Fetches data from the Last.fm API via a `GET` request.
 * @param payload - The parameters to be sent with the API request.
 * @returns {Promise<LastfmResponse | null>} The JSON response if the API call is successful, otherwise null.
 */
async function lastfmGet(payload: Record<string, any>) {
  try {
    const headers = { "User-Agent": payload.username };
    const url = "https://ws.audioscrobbler.com/2.0/";

    payload["api_key"] = process.env.REACT_APP_LASTFM_API_KEY!;
    payload["format"] = "json";

    const response = await axios.get(url, {
      headers,
      params: payload,
    });

    // Handle API-specific errors (e.g., invalid API key, missing parameters, etc)
    if (response.data.error) {
      return null;
    }

    return response.data as LastfmResponse;
  } catch (error) {
    return null;
  }
}

export default lastfmGet;
