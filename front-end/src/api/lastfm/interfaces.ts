interface Image {
  size: "small" | "medium" | "large" | "extralarge" | "mega" | "";
  "#text": string;
}

/**
 * `user.getInfo`
 * @see {@link https://www.last.fm/api/show/user.getInfo}
 * @see {@link https://lastfm-docs.github.io/api-docs/user/getInfo/}
 */
export interface LastfmUserInfo {
  age: string;
  album_count: string;
  artist_count: string;
  bootstrap: string;
  country: string;
  gender: string;
  image: Image[];
  name: string;
  playcount: string;
  playlists: string;
  realname: string;
  registered: {
    // account creation date
    "#text": string;
    unixtime: string;
  };
  subscriber: string;
  track_count: string;
  type: string;
  url: string;
}

export interface LastfmTrack {
  "@attr"?: { nowplaying: string };
  album: {
    "#text": string;
    mbid: string;
  };
  artist: {
    "#text": string;
    mbid: string;
  };
  date?: {
    "#text": string;
    uts: string;
  };
  image: Image[];
  mbid: string;
  name: string;
  streamable: string;
  url: string;
}

/**
 * `user.getRecentTracks`
 * @see {@link https://www.last.fm/api/show/user.getRecentTracks}
 * @see {@link https://lastfm-docs.github.io/api-docs/user/getRecentTracks/}
 */
export interface LastfmRecentTracks {
  "@attr": {
    page: string;
    perPage: string;
    total: string;
    totalPages: string;
    user: string;
  };
  track: LastfmTrack[];
}

interface WeeklyChartItem {
  from: string; // unix timestamp
  to: string;
}

/**
 * `user.getWeeklyChartList`
 * @see {@link https://www.last.fm/api/show/user.getWeeklyChartList}
 * @see {@link https://lastfm-docs.github.io/api-docs/user/getWeeklyChartList/}
 */
export interface LastfmWeeklyChartList {
  "@attr": {
    user: string;
  };
  chart: WeeklyChartItem[];
}

interface Link {
  "#text": string;
  href: string;
  rel: string;
}

interface SimilarArtist {
  image: Image[];
  name: string;
  url: string;
}

interface GenreTag {
  name: string;
  url: string;
}

/**
 * `artist.getInfo`
 * @see {@link https://www.last.fm/api/show/artist.getInfo}
 * @see {@link https://lastfm-docs.github.io/api-docs/artist/getInfo/}
 */
export interface LastfmArtistInfo {
  bio: {
    content: string;
    links: {
      link: Link;
    };
    published: string;
    summary: string;
  };
  image: Image[];
  mbid: string;
  name: string;
  ontour: string;
  similar: {
    artist: SimilarArtist[];
  };
  stats: {
    listeners: string;
    playcount: string;
  };
  streamable: string;
  tags: {
    tag: GenreTag[];
  };
  url: string;
}

export interface LastfmResponse {
  user: LastfmUserInfo;
  recenttracks: LastfmRecentTracks;
  weeklychartlist: LastfmWeeklyChartList;
  artist: LastfmArtistInfo;
}
