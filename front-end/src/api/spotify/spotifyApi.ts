import axios, { AxiosError } from "axios";
import { SpotifyAccessToken, SpotifyCatalog, SpotifyTrack } from "./interfaces";
import { parseJSON } from "utils/json";
import { LocalStorage } from "utils/localStorage";

/**
 * Structure of the Spotify Access Token that's saved in localStorage.
 * @property {string} token - The access token string used for authorization.
 * @property {number} expiresAt - The expiration time of the token, represented as a Unix timestamp.
 */
export interface StoredToken {
  token: string;
  expiresAt: number; // Unix timestamp
}

/**
 * Retrieves the Spotify Access Token stored in localStorage.
 */
export const getStoredToken = (): StoredToken | null => {
  const stringifiedToken = LocalStorage.get(
    LocalStorage.KEYS.SPOTIFY_ACCESS_TOKEN
  );
  const storedToken = parseJSON<StoredToken>(stringifiedToken);
  return storedToken;
};

/**
 * Fetches a new Spotify Access Token using the Client Credentials Flow.
 *
 * This function will:
 * 1. Attempt to fetch a new access token from the Spotify API.
 * 2. Save the newly generated access token to `localStorage` under the key `KEYS.SPOTIFY_ACCESS_TOKEN`.
 * 3. Return the access token (of type `SpotifyAccessToken`) if the API call is successful, otherwise return null.
 *
 * @returns {Promise<SpotifyAccessToken | null>} The Spotify access token if the API call is successful, otherwise null.
 *
 * @see {@link https://developer.spotify.com/documentation/web-api/tutorials/client-credentials-flow}
 */
export const getSpotifyAccessToken = async () => {
  try {
    const CLIENT_ID = process.env.REACT_APP_SPOTIFY_CLIENT_ID!;
    const CLIENT_SECRET = process.env.REACT_APP_SPOTIFY_CLIENT_SECRET!;

    const headers = {
      "Content-Type": "application/x-www-form-urlencoded",
      Authorization: "Basic " + btoa(CLIENT_ID + ":" + CLIENT_SECRET),
    };

    const body = new URLSearchParams({
      grant_type: "client_credentials",
    });

    const TOKEN_ENDPOINT = "https://accounts.spotify.com/api/token";
    const response = await axios.post(TOKEN_ENDPOINT, body, { headers });

    if (response.status !== 200 || response.data.error) {
      return null;
    }

    const data = response.data as SpotifyAccessToken;

    const { access_token, expires_in } = data;
    const expiresAt = Date.now() + expires_in * 1_000; // Unix timestamp

    LocalStorage.save(
      LocalStorage.KEYS.SPOTIFY_ACCESS_TOKEN,
      JSON.stringify({ token: access_token, expiresAt })
    );

    return data;
  } catch (error) {
    return null;
  }
};

/**
 * Wraps API logic to handle `401: Expired Token` errors, automatically refreshing the token and retrying the request if it fails.
 *
 * @param requestFunc - The API request function to be wrapped and retried in case of an expired token.
 * @returns {Promise<T | null>} The response data or null if the request fails after token refresh.
 */
const wrapWithTokenRefresh = async <T>(
  requestFunc: () => Promise<T>
): Promise<T | null> => {
  try {
    return await requestFunc();
  } catch (error) {
    const EXPIRED_TOKEN_ERR_CODE = 401;
    if ((error as AxiosError).response?.status === EXPIRED_TOKEN_ERR_CODE) {
      await getSpotifyAccessToken(); // fetch a new access token
      return await requestFunc(); // retry the request
    }
    return null; // return null for errors that aren't 401
  }
};

/**
 * Get Spotify catalog information for a single track identified by its unique Spotify ID.
 *
 * @param id - The Spotify ID of the track.
 * @returns {Promise<SpotifyTrack | null>} The track information if the API call is successful, otherwise null.
 *
 * @see {@link https://developer.spotify.com/documentation/web-api/reference/get-track}
 */
export const getSpotifyTrackInfoById = async (
  id: string
): Promise<SpotifyTrack | null> => {
  // retrieve Access Token from `localStorage`
  const storedToken = getStoredToken();
  if (!storedToken || !storedToken.token) {
    return null;
  }
  return await wrapWithTokenRefresh(async () => {
    const TRACK_ENDPOINT = `https://api.spotify.com/v1/tracks/${id}`;
    const response = await axios.get(TRACK_ENDPOINT, {
      headers: { Authorization: `Bearer ${storedToken.token}` },
    });
    return response.data as SpotifyTrack;
  });
};

/**
 * Get Spotify catalog information about albums, artists, and/or tracks that match a keyword string.
 *
 * @param {string} q - The search query with optional field filters, e.g., `artist:Taylor Swift`.
 * @param {string} type - Item types to search: `"artist"`, `"album"`, `"track"`, or a comma-separated list (e.g., `"album,track"`).
 * @param {number} [limit=1] - The maximum number of results per type. Defaults to 1.
 * @returns {Promise<SpotifyCatalog | null>} The catalog information if the API call is successful, otherwise null.
 *
 * @see {@link https://developer.spotify.com/documentation/web-api/reference/search}
 */
export const getSpotifyCatalogInfo = async (
  q: string,
  type: string,
  limit: number = 1
): Promise<SpotifyCatalog | null> => {
  // retrieve Access Token from `localStorage`
  const storedToken = getStoredToken();
  if (!storedToken || !storedToken.token) {
    return null;
  }
  return await wrapWithTokenRefresh(async () => {
    const SEARCH_ENDPOINT = "https://api.spotify.com/v1/search";
    const response = await axios.get(SEARCH_ENDPOINT, {
      params: {
        q,
        type,
        market: "US",
        limit,
        offset: 0,
      },
      headers: { Authorization: `Bearer ${storedToken.token}` },
    });
    return response.data as SpotifyCatalog;
  });
};
