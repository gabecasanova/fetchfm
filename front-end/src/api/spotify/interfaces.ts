/**
 * @see {@link https://developer.spotify.com/documentation/web-api/tutorials/client-credentials-flow}
 */
export interface SpotifyAccessToken {
  access_token: string;
  expires_in: number;
  token_type: string;
}

interface Image {
  url: string;
  height: number | null;
  width: number | null;
}

interface SimplifiedSpotifyArtist {
  external_urls?: {
    spotify?: string;
  };
  href?: string;
  id?: string;
  name?: string;
  type?: "artist";
  uri?: string;
}

/**
 * @see {@link https://developer.spotify.com/documentation/web-api/reference/get-an-artist}
 */
interface SpotifyArtist {
  external_urls?: {
    spotify?: string;
  };
  followers?: {
    href?: string | null;
    total?: number;
  };
  genres?: string[];
  href?: string;
  id?: string;
  images?: Image[];
  name?: string;
  popularity?: number; // [0,100], with 100 being the most popular
  type?: "artist";
  uri?: string;
}

/**
 * @see {@link https://developer.spotify.com/documentation/web-api/reference/get-an-album}
 */
interface SpotifyAlbum {
  album_type: string;
  total_tracks: number;
  available_markets: string[];
  external_urls: {
    spotify?: string;
  };
  href: string;
  id: string;
  images: Image[];
  name: string;
  release_date: string;
  release_date_precision: "year" | "month" | "day";
  restrictions?: {
    reason?: "market" | "product" | "explicit";
  };
  type: "album";
  uri: string;
  artists: SimplifiedSpotifyArtist[];
}

/**
 * @see {@link https://developer.spotify.com/documentation/web-api/reference/get-track}
 */
export interface SpotifyTrack {
  album?: SpotifyAlbum;
  artists?: SimplifiedSpotifyArtist[];
  available_markets?: string[];
  disc_number?: number;
  duration_ms?: number;
  explicit?: boolean;
  external_ids?: {
    isrc?: string;
    ean?: string;
    upc?: string;
  };
  external_urls?: {
    spotify?: string;
  };
  href?: string;
  id?: string;
  id_playable?: boolean;
  linked_from?: {};
  restrictions?: {
    reason?: "market" | "product" | "explicit";
  };
  name?: string;
  popularity?: number; // [0,100], with 100 being the most popular
  preview_url?: string | null;
  track_number?: number;
  type?: "track";
  uri?: string;
  is_local?: boolean;
}

/**
 * @see {@link https://developer.spotify.com/documentation/web-api/reference/search}
 */
export interface SpotifyCatalog {
  albums?: {
    href: string;
    limit: number;
    next: string | null;
    offset: number;
    previous: string | null;
    total: number;
    items: SpotifyAlbum[];
  };
  artists?: {
    href: string;
    limit: number;
    next: string | null;
    offset: number;
    previous: string | null;
    total: number;
    items: SpotifyArtist[];
  };
  tracks?: {
    href: string;
    limit: number;
    next: string | null;
    offset: number;
    previous: string | null;
    total: number;
    items: SpotifyTrack[];
  };
}

export interface SpotifyResponse {
  access_token: SpotifyAccessToken;
  track: SpotifyTrack;
  catalog: SpotifyCatalog;
}
