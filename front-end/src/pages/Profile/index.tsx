import { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import lastfmGet from "api/lastfm/lastfmApi";
import { LastfmUserInfo } from "api/lastfm/interfaces";
// Child Components & Styling
import NavBar from "./components/NavBar";
import ExpandedHeader from "./components/ExpandedHeader";
import Overview from "./Overview";
import "./Profile.css";

const Profile = () => {
  const { username } = useParams();
  const navigate = useNavigate();

  const [userInfo, setUserInfo] = useState<LastfmUserInfo | null>(null);
  const [loadingUserInfo, setLoadingUserInfo] = useState<boolean>(true);
  const [errorUserInfo, setErrorUserInfo] = useState<string | null>(null);

  /* Maintains state for the currently rendered tab content of this single-page app */
  const tabs = ["Overview", "Reports", "Statistics", "Library"];
  const [activeTab, setActiveTab] = useState(tabs[0]);

  useEffect(() => {
    const fetchUserInfo = async () => {
      try {
        const response = await lastfmGet({
          method: "user.getInfo",
          user: username,
        });
        setUserInfo(response!.user);
        const recentTrack = await lastfmGet({
          method: "user.getRecentTracks",
          limit: 1,
          user: username,
        });
        if (!recentTrack) {
          // navigate to Login if user's "Recent listening" setting isn't enabled
          navigate("/");
        }
      } catch (err) {
        // navigate to Login if provided Last.fm username doesn't exist
        navigate("/");
        setErrorUserInfo(`Failed to fetch user info: ${err}`);
      } finally {
        setLoadingUserInfo(false);
      }
    };
    fetchUserInfo();
  }, [username, navigate]);

  if (loadingUserInfo) return <div />;

  if (errorUserInfo) {
    return <div>{errorUserInfo}</div>;
  }

  return (
    <div className="profile">
      <NavBar profilepic={userInfo?.image[0]["#text"]!} />
      <ExpandedHeader
        username={username!}
        userInfo={userInfo!}
        tabs={tabs}
        activeTab={activeTab}
        setActiveTab={setActiveTab}
      />
      {activeTab === tabs[0] && (
        <Overview username={username!} userInfo={userInfo!} />
      )}
    </div>
  );
};

export default Profile;
