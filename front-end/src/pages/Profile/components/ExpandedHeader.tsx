import { motion } from "framer-motion";
import { LastfmUserInfo } from "api/lastfm/interfaces";
import { defaultProfilePic } from "assets/images";
import { getDateStringFromUnix } from "utils/date";
import useScreenWidth from "hooks/useScreenWidth";
import "./ExpandedHeader.css";

interface Props {
  username: string;
  userInfo: LastfmUserInfo;
  tabs: string[];
  activeTab: string;
  setActiveTab: React.Dispatch<React.SetStateAction<string>>;
}

const ExpandedHeader = ({
  username,
  userInfo,
  tabs,
  activeTab,
  setActiveTab,
}: Props) => {
  const realname = userInfo.realname!;
  const profilepic = userInfo.image[3]["#text"]!;
  const date = getDateStringFromUnix(userInfo.registered.unixtime!);
  const screenWidth = useScreenWidth();

  const cn = "expanded-header";
  return (
    <div className={cn}>
      <div className={`${cn}__bg`} />
      <div className={`${cn}__container`}>
        <img
          src={profilepic || defaultProfilePic}
          alt={`${username}'s profile`}
          className={`${cn}__profile-picture`}
          draggable={false}
        />
        <a
          className={`${cn}__username`}
          href={`https://www.last.fm/user/${username}`}
          target="_blank"
          rel="noreferrer"
        >
          {username}
        </a>
        <span className={`${cn}__info-line`}>
          {screenWidth < 400
            ? `${date}`
            : `${
                realname
                  ? `${realname} • scrobbling since ${date}`
                  : `Scrobbing since ${date}`
              }`}
        </span>
        <div className={`${cn}__tabs-container`}>
          {tabs.map((tab) => (
            <button
              key={tab}
              onClick={() => setActiveTab(tab)}
              className={`${cn}__tab-button ${
                activeTab !== tab ? "not-active" : ""
              }`}
            >
              {/* Text inside the tabs */}
              <span className={`${cn}__button-text`}>{tab}</span>
              {activeTab === tab && (
                <motion.div
                  layoutId="active-tab"
                  className={`${cn}__button-selector`}
                />
              )}
            </button>
          ))}
        </div>
      </div>
    </div>
  );
};

export default ExpandedHeader;
