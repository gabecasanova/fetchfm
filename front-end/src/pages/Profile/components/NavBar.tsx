import { Button, Container, Navbar } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { defaultProfilePic } from "assets/images";
import "./NavBar.css";

interface Props {
  profilepic: string;
}

const NavBar = ({ profilepic }: Props) => {
  const navigate = useNavigate();

  const __DISPLAY_PROFILE_PIC = false;

  return (
    <Navbar className="navbar">
      <Container className="navbar__inner-container">
        <Navbar.Brand
          href="/"
          className="navbar__brand"
          onClick={() => navigate("/")}
        >
          <span className="navbar__name">Lume.fm</span>
        </Navbar.Brand>
        {__DISPLAY_PROFILE_PIC && (
          <div className="navbar__button-wrapper">
            {/* User's Profile */}
            <Button className="navbar__button">
              <img src={profilepic || defaultProfilePic} alt="User's profile" />
            </Button>
          </div>
        )}
      </Container>
    </Navbar>
  );
};

export default NavBar;
