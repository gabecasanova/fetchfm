import { useEffect, useRef } from "react";
import * as d3 from "d3";
import { isMobile } from "react-device-detect";
import { getDOY } from "utils/date";
import { LocalStorage } from "utils/localStorage";
import "./YearlyGridContainer.css";

interface Props {
  username: string;
  year: number;
  dayOfTheYear: number;
  isCellSelected: boolean;
  accentColorByDate: Record<string, string>;
  loading: boolean | null;
  accountCreationDate: Date;
  setDayOfTheYear: React.Dispatch<React.SetStateAction<number>>;
  setIsHovering: React.Dispatch<React.SetStateAction<boolean>>;
  setIsCellSelected: React.Dispatch<React.SetStateAction<boolean>>;
  setLoading: React.Dispatch<React.SetStateAction<boolean | null>>;
}

interface GridData {
  id: number;
  x: number;
  y: number;
  width: number;
  height: number;
}

const YearlyGridContainer = ({
  username,
  year,
  dayOfTheYear,
  isCellSelected,
  accentColorByDate,
  loading,
  accountCreationDate,
  setDayOfTheYear,
  setIsHovering,
  setIsCellSelected,
  setLoading,
}: Props) => {
  const CELL_SIZE = 24; // width, height
  const CELL_PADDING = 5.5; // padding b/w adjacent cells
  const CELL_STROKE = 2;
  const CELL_COLOR = "#dcdcdc";
  const CELL_GROWTH_FACTOR = 1.45;
  const TRANSITION_DURATION = 85; // in milliseconds

  // used to delay the derendering of the tooltip when users aren't hovering over a cell
  const hideTooltipTimer = useRef<ReturnType<typeof setTimeout> | null>(null);

  let data: GridData[][] = gridData(year, CELL_SIZE); // 2D-array

  useEffect(() => {
    const grid = d3.select(".yearly-grid");
    grid.selectAll("svg").remove();

    const svg = grid
      .append("svg")
      .attr("width", 583)
      .attr("height", 390)
      .style("cursor", "pointer")
      .style("margin-top", -5);

    const row = svg
      .selectAll(".yearly-grid__row")
      .data(data)
      .enter()
      .append("g")
      .attr("class", "yearly-grid__row")
      .style("transform", "translate(0.5%, 0.5%)"); // shift to prevent clipping

    const cell = row
      .selectAll(".yearly-grid__cell")
      .data((d: GridData[]) => d)
      .enter()
      .append("rect")
      .attr("class", "yearly-grid__cell");

    cell
      .attr("x", (d) => d.x + CELL_PADDING / 2)
      .attr("y", (d) => d.y + CELL_PADDING / 2)
      .attr("width", (d) => d.width - CELL_PADDING)
      .attr("height", (d) => d.height - CELL_PADDING)
      .style("fill", (d) => {
        const dateKey = `accentColorOn:${username}_${d.id}_${year}`;
        return (
          LocalStorage.get(dateKey) || accentColorByDate[dateKey] || CELL_COLOR
        );
      })
      .style("stroke-width", 0)
      .style("stroke", function () {
        const fillColor = d3.select(this).style("fill");
        return d3.color(fillColor)!.darker(0.4).formatHex();
      })
      .style(
        "transition",
        `transform ${TRANSITION_DURATION}ms ease-out, rx ${TRANSITION_DURATION}ms ease-out, ry ${TRANSITION_DURATION}ms ease-out`
      );

    // MouseEvents
    cell
      .on("click", function (_, d) {
        // if no cell is selected, select the current cell
        if (!isCellSelected) {
          if (isMobile) {
            setIsHovering(true);
            setDayOfTheYear(d.id);
          }
          setIsCellSelected(true); // causing "Top Stats" to render
          return;
        }
        // if user is clicking on currently selected cell, deselect it
        if (d.id === dayOfTheYear && !loading) {
          if (isMobile) {
            setIsHovering(false);
            setDayOfTheYear(-1);
          }
          setIsCellSelected(false);
          setLoading(null); // null b/c no ongoing process of fetching data
        }
      })
      .on("mouseover", function (_, d) {
        // if no cell is selected, update hover status to true
        if (!isCellSelected) {
          if (hideTooltipTimer.current) {
            clearTimeout(hideTooltipTimer.current); // clear previous timer
          }
          setIsHovering(true);
          setDayOfTheYear(d.id);

          const cx = d.x + d.width / 2;
          const cy = d.y + d.height / 2;

          d3.select(this)
            .style("stroke-width", CELL_STROKE)
            .style("transform-origin", `${cx}px ${cy}px`)
            .style("transform", `scale(${CELL_GROWTH_FACTOR})`)
            .attr("rx", CELL_PADDING / 2)
            .attr("ry", CELL_PADDING / 2);
          return;
        }
        // if user is hovering over the currently selected cell, lower opacity
        if (d.id === dayOfTheYear) {
          d3.select(this).style("opacity", "75%");
        }
      })
      .on("mouseout", function (_, d) {
        // if no cell is currently selected, update hover status to false
        if (!isCellSelected) {
          hideTooltipTimer.current = setTimeout(() => {
            // delay hiding the toolip to account for movement b/w adjacent cells
            setIsHovering(false);
            setDayOfTheYear(-1);
          }, 125);

          const cx = d.x + d.width / 2;
          const cy = d.y + d.height / 2;

          d3.select(this)
            .style("stroke-width", 0)
            .style("transform-origin", `${cx}px ${cy}px`)
            .style("transform", "scale(1)")
            .attr("rx", 0)
            .attr("ry", 0);
          return;
        }
        // if user is hovering away from the currently selected cell, reset opacity
        if (d.id === dayOfTheYear) {
          d3.select(this).style("opacity", "100%");
        }
      });

    // hide all cells that come BEFORE the user's account creation date
    if (year === accountCreationDate.getFullYear()) {
      const doy = getDOY(accountCreationDate);
      d3.selectAll(".yearly-grid__cell")
        .filter(function (datum) {
          const d = datum as GridData;
          return d.id < doy;
        })
        .style("display", "none");
    }

    // if a cell is selected, give all other cells a 'disabled' look
    if (isCellSelected) {
      d3.selectAll(".yearly-grid__cell").each(function (datum) {
        const d = datum as GridData;
        const cell = d3.select(this);
        if (d.id === dayOfTheYear) {
          // selected cell
          const cx = d.x + d.width / 2;
          const cy = d.y + d.height / 2;
          cell
            .style("stroke-width", CELL_STROKE)
            .style("transform-origin", `${cx}px ${cy}px`)
            .style("transform", `scale(${CELL_GROWTH_FACTOR})`)
            .attr("rx", CELL_PADDING / 2)
            .attr("ry", CELL_PADDING / 2);
        } else {
          // all other cells
          const fillColor: string = cell.style("fill");
          cell.style("fill", `rgba(${fillColor.slice(4, -1)}, 0.3)`);
        }
      });
    }
    // eslint-disable-next-line
  }, [year, dayOfTheYear, isCellSelected, loading, accentColorByDate]);

  return <div className="yearly-grid" />;
};

/* Generates a 2D-array of grid cells ("days") for the given year */
const gridData = (year: number, CELL_SIZE: number): GridData[][] => {
  const isLeapYear = (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
  const isCurrentYear = year === new Date().getFullYear(); // is `year` equal to the current year?

  const CELLS_PER_ROW = 24;
  const NUM_DAYS_IN_YEAR = isCurrentYear
    ? getDOY(new Date())
    : 365 + Number(isLeapYear);

  let res: GridData[][] = [];
  let xpos = 1;
  let ypos = 1;

  let cellId = 1; // will represent dayOfTheYear (1-based, i.e., Janurary 1st is 1)
  let totalRows = Math.ceil(NUM_DAYS_IN_YEAR / CELLS_PER_ROW);

  // iterate over the rows
  for (let row = 0; row < totalRows; row++) {
    res.push([]);

    const isLastRow = row === totalRows - 1;
    let numCells = isLastRow ? NUM_DAYS_IN_YEAR % CELLS_PER_ROW : CELLS_PER_ROW;

    // iterate over the cells within the row
    for (let col = 0; col < numCells; col++) {
      res[row].push({
        id: cellId++,
        x: xpos,
        y: ypos,
        width: CELL_SIZE,
        height: CELL_SIZE,
      });
      xpos += CELL_SIZE;
    }
    xpos = 1;
    ypos += CELL_SIZE;
  }
  return res;
};

export default YearlyGridContainer;
