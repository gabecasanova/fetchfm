import { useState, useEffect, useCallback } from "react";
import { MdKeyboardArrowLeft, MdKeyboardArrowRight } from "react-icons/md";
import { ScaleLoader } from "react-spinners";
import { isMobile } from "react-device-detect";
// API Calls
import lastfmGet from "api/lastfm/lastfmApi";
import { LastfmTrack } from "api/lastfm/interfaces";
import { LastfmTopStats, getTopStats } from "api/lastfm/helpers/topStats";
import { getLastfmScrobblesUrlForDate } from "api/lastfm/helpers/lastfmUrls";
// Utils
import { getDailyUnixTimestamps, getWeekdayDateFromDOY } from "utils/date";
import { getAccentColor } from "utils/colors";
import { LocalStorage } from "utils/localStorage";
// Child Components & Styling
import YearlyGridContainer from "./YearlyGridContainer";
import "./YearAtAGlance.css";

interface Props {
  username: string;
  accountCreationDate: Date;
}

const YearAtAGlance = ({ username, accountCreationDate }: Props) => {
  const ACCOUNT_CREATION_YEAR = accountCreationDate.getFullYear();
  const CURRENT_YEAR = new Date().getFullYear();

  const [year, setYear] = useState<number>(CURRENT_YEAR);
  const [dayOfTheYear, setDayOfTheYear] = useState<number>(-1); // currently selected cell ID (1-based)
  const [isHovering, setIsHovering] = useState(false);
  const [isCellSelected, setIsCellSelected] = useState(false);

  // State management for Last.fm API results using `fetchDailyTracks()`
  const [dailyTracks, setDailyTracks] = useState<LastfmTrack[] | null>(null);
  const [loading, setLoading] = useState<boolean | null>(null); // null b/c not currently fetching dailyTracks
  const [error, setError] = useState<string | null>(null);

  // Stores data for the user's most listened-to artist, album, and song
  const [topStats, setTopStats] = useState<LastfmTopStats | null>(null);

  // Maps a combined "dayOfTheYear_year" key to its associated "accentColor" (based on 'Top Album')
  const [accentColorByDate, setAccentColorByDate] = useState<
    Record<string, string>
  >({});

  useEffect(() => {
    if (error) {
      console.log(error);
    }
  }, [error]);

  const fetchDailyTracks = useCallback(async () => {
    setLoading(true);

    const { from, to } = getDailyUnixTimestamps(year, dayOfTheYear);
    const selectedDate = new Date(year, 0, dayOfTheYear);
    try {
      const initialPayload = {
        method: "user.getRecentTracks",
        from,
        to,
        user: username,
        limit: 200,
      };

      let response = await lastfmGet(initialPayload);

      if (!response) {
        setIsCellSelected(false);
        return;
      }

      // Handles multi-page API results (if scrob count exceeds LIMIT of 200 per request)
      const TOTAL_PAGES = Number(response["recenttracks"]["@attr"].totalPages);

      if (TOTAL_PAGES === 0) {
        // user didn't listen to any music on the selected day
        await new Promise((resolve) => setTimeout(resolve, 500)); // brief delay to allow loading animation to run

        const dateKey = `accentColorOn:${username}_${dayOfTheYear}_${year}`;
        LocalStorage.save(dateKey, "#fff"); // white

        setAccentColorByDate((prev) => ({
          ...prev,
          [dateKey]: "#fff",
        }));

        setDailyTracks([]);
        return;
      }

      let tracks: LastfmTrack[] | null = null;
      let page = 0;

      while (++page <= TOTAL_PAGES) {
        response = await lastfmGet({
          ...initialPayload,
          page,
        });

        if (!response) {
          continue;
        }

        // Don't consider "now playing" scrob b/c Last.fm doesn't consider them
        const filteredTracks = response.recenttracks.track.filter(
          (scrob) => !scrob["@attr"]?.nowplaying
        );

        if (!tracks) {
          tracks = filteredTracks; // initialize with first set of tracks
        } else {
          tracks.push(...filteredTracks); // spread to add all tracks
        }
      }

      try {
        const topStats: LastfmTopStats = await getTopStats(tracks!);

        const dateKey = `accentColorOn:${username}_${dayOfTheYear}_${year}`;
        const accentColor = await getAccentColor(topStats.album.image);

        LocalStorage.save(dateKey, accentColor);

        setAccentColorByDate((prev) => ({
          ...prev,
          [dateKey]: accentColor,
        }));

        setTopStats(topStats);
      } catch (err) {
        setError(
          `Failed to calculate user's Top Artist for ${selectedDate.toDateString()}`
        );
      }

      setDailyTracks(tracks);
    } catch (err) {
      setError(
        `Failed to fetch user's daily tracks for ${selectedDate.toDateString()}`
      );
    } finally {
      setLoading(false);
    }
  }, [dayOfTheYear, year, username]);

  // If the user has just selected a NEW grid cell, run Last.fm API request
  useEffect(() => {
    if (isCellSelected) {
      fetchDailyTracks();
    } else {
      // user deselected grid cell, set old state values back to null
      setTopStats(null);
      setDailyTracks(null);
    }
  }, [isCellSelected, fetchDailyTracks]);

  // -=====================================================-
  // -===  Helper Functions for Rendering JSX Elements  ===-
  // -=====================================================-

  const navigationArrows = () => {
    return (
      <span style={{ marginTop: 20 }}>
        <MdKeyboardArrowLeft
          className={`yearly-stats__arrow${
            isCellSelected || year === ACCOUNT_CREATION_YEAR ? "--disabled" : ""
          }`}
          style={{ marginRight: 7.5 }}
          onClick={() => {
            if (!isCellSelected && year !== ACCOUNT_CREATION_YEAR)
              setYear(year - 1);
          }}
        />
        <MdKeyboardArrowRight
          className={`yearly-stats__arrow${
            isCellSelected || year === CURRENT_YEAR ? "--disabled" : ""
          }`}
          onClick={() => {
            if (!isCellSelected && year !== CURRENT_YEAR) setYear(year + 1);
          }}
        />
      </span>
    );
  };

  const loader = () => {
    // display loading animation while waiting for data to load
    return (
      <span className="loader">
        <ScaleLoader
          height={40}
          width={6}
          margin={5}
          radius={5}
          style={{ transform: "translateY(15px)" }}
        />
      </span>
    );
  };

  const scrobCount = (count: number) => {
    return (
      <span style={{ fontWeight: "normal", fontSize: 24 }}>
        {`: ${count} scrobble${count === 1 ? "" : "s"}`}
      </span>
    );
  };

  const tooltip = () => {
    return (
      <div className="yearly-stats__tooltip">
        <a
          className="yearly-stats__tooltip-date"
          href={getLastfmScrobblesUrlForDate(username, dayOfTheYear, year)}
          target="_blank"
          rel="noreferrer"
        >
          {getWeekdayDateFromDOY(year, dayOfTheYear)}
        </a>
        {/* 'Scrob Count & Top Stats' visibility */}
        {isCellSelected && (
          <>
            {!dailyTracks && !topStats
              ? loader()
              : scrobCount(dailyTracks!.length)}
            {/* If data exists, render 'Top Stats' */}
            {dailyTracks && dailyTracks!.length > 0 && (
              <div className="yearly-stats__tooltip-top-stats">
                {Object.values(topStats!).map((item, index) => (
                  <TooltipCard
                    type={item.type}
                    name={item.name}
                    img={item.image}
                    url={item.url}
                    count={item.count}
                    key={index}
                  />
                ))}
              </div>
            )}
          </>
        )}
      </div>
    );
  };

  return (
    <div className="yearly-stats">
      <div className="header-container">
        <h1 className="title">{year} at a Glance</h1>
        {navigationArrows()}
      </div>
      <YearlyGridContainer
        username={username}
        year={year}
        dayOfTheYear={dayOfTheYear}
        isCellSelected={isCellSelected}
        accentColorByDate={accentColorByDate}
        loading={loading}
        accountCreationDate={accountCreationDate}
        setDayOfTheYear={setDayOfTheYear}
        setIsHovering={setIsHovering}
        setIsCellSelected={setIsCellSelected}
        setLoading={setLoading}
      />
      {/* 'Tooltip' visibility based on device type */}
      {isMobile ? isCellSelected && tooltip() : isHovering && tooltip()}
    </div>
  );
};

interface TooltipCardProps {
  type: string;
  name: string;
  img: string;
  url: string;
  count: number;
}

const TooltipCard = ({ type, name, img, url, count }: TooltipCardProps) => {
  const TEXT_PADDING = 10;
  const CARD_SIZE = 140;

  const defaultStyle = {
    width: CARD_SIZE,
  };

  return (
    <div className="yearly-stats__tooltip-card-wrapper" style={defaultStyle}>
      <div className="yearly-stats__tooltip-card-header" style={defaultStyle}>
        {/* Hidden by default, rendered on .tooltip-card-wrapper hover */}
        {/* e.g. "Top Artist: 47 sb" */}
        Top {type}:
        <span style={{ fontWeight: "normal", paddingLeft: 3 }}>{count} sb</span>
      </div>
      <div className="yearly-stats__tooltip-card">
        <img src={img} alt={name} width={CARD_SIZE} height={CARD_SIZE} />
        <div
          style={{
            width: CARD_SIZE - 2 * TEXT_PADDING,
            padding: `2.5px  ${TEXT_PADDING}px 7.5px ${TEXT_PADDING}px`,
            backgroundColor: "#181818",
            color: "white",
            borderRadius: "0 0 7px 7px",
            overflow: "hidden",
            textOverflow: "ellipsis",
            whiteSpace: "nowrap",
          }}
        >
          <a href={url} target="_blank" rel="noreferrer">
            {name}
          </a>
        </div>
      </div>
    </div>
  );
};

export default YearAtAGlance;

// https://gist.github.com/cagrimmett/07f8c8daea00946b9e704e3efcbd5739
