import { LastfmUserInfo } from "api/lastfm/interfaces";
import { getDateObjFromUnix } from "utils/date";
import { isMobile } from "react-device-detect";
// Child Components & Styling
import ScrobbleList from "./ScrobbleList";
import YearAtAGlance from "./YearAtAGlance";
import UserStats from "./UserStats";
import "./Overview.css";

interface Props {
  username: string;
  userInfo: LastfmUserInfo;
}

const Overview = ({ username, userInfo }: Props) => {
  const cn = "overview";
  return (
    <div className={cn}>
      {/* Top Row: `ScrobbleList` and `YearAtAGlance` */}
      <div className={`${cn}__top-row-wrapper`}>
        <div className={`${cn}__top-row`}>
          <ScrobbleList
            title="Scrobbles"
            username={username!}
            limit={50}
            playcount={Number(userInfo?.playcount!)}
          />
          {/* Hides 'YearAtAGlance' on mobile phones */}
          {!isMobile && (
            <YearAtAGlance
              username={username!}
              accountCreationDate={getDateObjFromUnix(
                userInfo.registered.unixtime!
              )}
            />
          )}
        </div>
      </div>
      {/* Second Row: `UserStats` */}
      <div className={`${cn}__second-row-wrapper`}>
        <UserStats userInfo={userInfo} />
      </div>
      {/* Third Row: `TopAlbums` */}
      <div className={`${cn}__third-row-wrapper`} />
    </div>
  );
};

export default Overview;
