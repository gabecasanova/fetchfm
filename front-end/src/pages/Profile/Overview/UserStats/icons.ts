import { IoLibrary } from "react-icons/io5";
import { FaStar } from "react-icons/fa";
import { BiSolidAlbum } from "react-icons/bi";
import { PiMusicNotesFill } from "react-icons/pi";

const icons = {
  scrobbles: IoLibrary,
  artists: FaStar,
  albums: BiSolidAlbum,
  tracks: PiMusicNotesFill,
};

export default icons;
