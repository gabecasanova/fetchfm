import { useEffect, useRef, useState, useCallback } from "react";
// React Icons
import { IconType } from "react-icons";
import icons from "./icons";
// API Calls
import lastfmGet from "api/lastfm/lastfmApi";
import { LastfmUserInfo } from "api/lastfm/interfaces";
import { getLastfmLibraryUrl } from "api/lastfm/helpers/lastfmUrls";
// Child Components & Styling
import AnimatedCounter from "components/AnimatedCounter";
import "./UserStats.css";

interface Props {
  userInfo: LastfmUserInfo;
}

interface UserStat {
  type: string;
  value: string;
  url: string; // links to user's lastfm library
  icon: IconType;
}

const UserStats = ({ userInfo: initialUserInfo }: Props) => {
  const refetchRef = useRef<NodeJS.Timeout | null>(null);

  const [userInfo, setUserInfo] = useState<LastfmUserInfo>(initialUserInfo);
  const [stats, setStats] = useState<UserStat[]>(extractStats(userInfo));

  // Updates `userInfo` via the "user.getInfo" method
  const fetchUserInfo = useCallback(async () => {
    try {
      const response = await lastfmGet({
        method: "user.getInfo",
        user: initialUserInfo.name,
      });
      setUserInfo(response!.user);
    } catch (error) {
      console.error("Error fetching user info:", error);
    }
  }, [initialUserInfo]);

  // Handles initial fetch and periodic refresh of `userInfo`
  useEffect(() => {
    // Setup interval for periodic refetching
    if (!refetchRef.current) {
      refetchRef.current = setInterval(fetchUserInfo, 60_000); // 60 seconds
    }
    fetchUserInfo();
    // Cleanup interval on unmount or dependency change
    return () => {
      if (refetchRef.current) {
        clearInterval(refetchRef.current);
        refetchRef.current = null;
      }
    };
  }, [fetchUserInfo]);

  // Updates the rendered `stats` data whenever `userInfo` changes
  useEffect(() => {
    setStats(extractStats(userInfo));
  }, [userInfo]);

  return (
    <div className="user-stats">
      {stats.map((stat, index) => (
        <div key={index} className="user-stats__stat">
          <div className="user-stats__stat-header">
            <stat.icon />
            {stat.type}
          </div>
          <div className="user-stats__stat-value">
            <a href={stat.url} target="_blank" rel="noreferrer">
              <AnimatedCounter
                from={Number(stat.value) / 2}
                to={Number(stat.value)}
              />
            </a>
          </div>
        </div>
      ))}
    </div>
  );
};

/**
 * Extracts relevant `UserStats` from the provided `userInfo` API data.
 *
 * @param {LastfmUserInfo} userInfo - The user data from Last.fm.
 * @returns {UserStat[]} An array of `UserStat` objects.
 */
const extractStats = (userInfo: LastfmUserInfo): UserStat[] => {
  const { name, playcount, artist_count, album_count, track_count } = userInfo;
  const libraryUrl = getLastfmLibraryUrl(name);
  return [
    {
      type: "Scrobbles",
      value: playcount,
      url: libraryUrl,
      icon: icons.scrobbles,
    },
    {
      type: "Artists",
      value: artist_count,
      url: `${libraryUrl}/artists`,
      icon: icons.artists,
    },
    {
      type: "Albums",
      value: album_count,
      url: `${libraryUrl}/albums`,
      icon: icons.albums,
    },
    {
      type: "Tracks",
      value: track_count,
      url: `${libraryUrl}/tracks`,
      icon: icons.tracks,
    },
  ];
};

export default UserStats;
