import { useState, useEffect, useRef, ReactNode } from "react";
import { FaSearch } from "react-icons/fa";
import { IoIosCheckmark, IoIosClose } from "react-icons/io";
import { BarLoader } from "react-spinners";
// Hooks, Assets, Utils
import useDebounce from "hooks/useDebounce";
import { defaultLastfmImages } from "assets/images";
import { statusColors } from "assets/colors";
import { getElapsedTimeSince } from "utils/date";
// API Calls
import lastfmGet from "api/lastfm/lastfmApi";
import { LastfmRecentTracks, LastfmTrack } from "api/lastfm/interfaces";
import {
  getLastfmArtistUrl,
  getLastfmAlbumUrl,
} from "api/lastfm/helpers/lastfmUrls";
// Child Components & Styling
import ListPager from "components/ListPager";
import "./ScrobbleList.css";

interface Props {
  title: string;
  username: string;
  limit: number;
  playcount: number;
}

const ScrobbleList = ({ title, username, limit, playcount }: Props) => {
  const [currentPage, setCurrentPage] = useState(1);
  const [pageInput, setPageInput] = useState("");
  const [alertColor, setAlertColor] = useState<string | null>(null); // used for searchbar button
  const [recentTracks, setRecentTracks] = useState<LastfmRecentTracks | null>(
    null
  );
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState<string | null>(null);

  const refetchRef = useRef<NodeJS.Timeout | null>(null);

  /**
   * @param {boolean} [shouldScroll=true] - Determines whether the table scroll
   * should be reset to the top of the component. Defaults to `true`.
   */
  const fetchRecentTracks = async (shouldScroll: boolean = true) => {
    try {
      const payload: { [key: string]: any } = {
        method: "user.getRecentTracks",
        limit,
        page: currentPage,
        user: username,
      };
      const response = await lastfmGet(payload);

      if (!response) {
        return;
      }

      // Filter out the "now playing" scrobble if currentPage is not 1
      const filtered = {
        ...response.recenttracks,
        track:
          currentPage !== 1
            ? response.recenttracks.track.filter(
                (scrob) => !scrob["@attr"]?.nowplaying
              )
            : response.recenttracks.track,
      };
      setRecentTracks(filtered);

      // Set up interval to auto-fetch recent tracks if "now playing" track is present
      if (filtered.track.some((scrob) => scrob["@attr"]?.nowplaying)) {
        if (!refetchRef.current) {
          // If there's not yet an existing interval, start a new one
          const DELAY_IN_MS = 10_000;
          refetchRef.current = setInterval(
            () => fetchRecentTracks(false), // ensures table isn't scrolled to top
            DELAY_IN_MS
          );
        }
      } else if (refetchRef.current) {
        // If there's no "now playing" track but an interval exists, clear it
        clearInterval(refetchRef.current);
        refetchRef.current = null;
      }
    } catch (err) {
      setError("Failed to fetch user recent tracks");
    } finally {
      if (shouldScroll) {
        // On page change only, ensure scrobble table is scrolled to the top
        const tableContainer = document.querySelector(
          ".scrobble-list__table-container"
        );
        if (tableContainer) {
          tableContainer.scrollTop = 0;
        }
      }
      setLoading(false);
    }
  };

  // fetchRecentTracks() wrapped within useDebounce hook
  const debouncedFetch = useDebounce({
    callback: fetchRecentTracks,
    delay: 200,
  });

  useEffect(() => {
    // minimizes excessive API calls caused by rapid pagination changes
    debouncedFetch();
    return () => {
      if (refetchRef.current) {
        clearInterval(refetchRef.current);
        refetchRef.current = null;
      }
    };
  }, [currentPage, debouncedFetch]);


  // Functions used for .page-jump and .list-pager
  const handlePageInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setPageInput(e.target.value);
  };

  const handlePageJump = () => {
    const pageNumber = parseInt(pageInput, 10);
    if (
      !isNaN(pageNumber) &&
      pageNumber >= 1 &&
      pageNumber <= Math.ceil(playcount / limit)
    ) {
      setCurrentPage(pageNumber);
      setAlertColor(statusColors.SUCCESS);
    } else if (pageInput !== "") {
      // invalid search query (out of bounds)
      setAlertColor(statusColors.ERROR);
    }
    setTimeout(() => setAlertColor(null), 800);
    setPageInput("");
  };

  /**
   * Generates an anchor (`<a>`) element.
   * @param href - The URL the link points to.
   * @param child - The content inside the link.
   * @returns A link (`<a>`) element with the specified `href` and `child`.
   */
  function getExternalLink(href: string, child: ReactNode | string) {
    return (
      <a href={href} target="_blank" rel="noreferrer">
        {child}
      </a>
    );
  }

  /**
   * @remarks Visible by default
   */
  const generateExpandedView = (scrob: LastfmTrack) => {
    return (
      <>
        <td className="expanded-track-info__song">
          {getExternalLink(scrob.url, scrob.name)}
        </td>
        <td className="expanded-track-info__artist">
          {getExternalLink(
            getLastfmArtistUrl(scrob.url),
            scrob.artist["#text"]
          )}
        </td>
        <td className="expanded-track-info__time-scrobbled">
          {scrob["@attr"]?.nowplaying ? (
            <span className="expanded-track-info__time-scrobbled--now">
              Scrobbling now
            </span>
          ) : (
            (() => {
              const time = getElapsedTimeSince(scrob.date?.uts!);
              if (time.includes("|")) {
                // This scrobble is from a previous year, stack date and time to conserve space
                const [dateString, timeString] = time.split("|");
                return (
                  <span>
                    <div>{dateString}</div>
                    <div style={{ fontSize: "0.95em", color: "#787878" }}>
                      {timeString}
                    </div>
                  </span>
                );
              }
              // Scrobble is from the current year, return default formatting
              return time;
            })()
          )}
        </td>
      </>
    );
  };

  /**
   * @remarks Hidden by default
   */
  const generateStackedView = (scrob: LastfmTrack) => {
    return (
      <td className="stacked-track-info">
        <table>
          <tbody>
            <tr className="stacked-track-info__song">
              <td>{getExternalLink(scrob.url, scrob.name)}</td>
            </tr>
            <tr className="stacked-track-info__details">
              <td>
                {/* artist name */}
                {getExternalLink(
                  getLastfmArtistUrl(scrob.url),
                  scrob.artist["#text"]
                )}
                {/* album name */}
                {scrob.album["#text"] && (
                  <>
                    <span
                      style={{
                        paddingRight: 6,
                        paddingLeft: 6,
                        userSelect: "none",
                      }}
                    >
                      •
                    </span>
                    {getExternalLink(
                      getLastfmAlbumUrl(scrob.album["#text"], scrob.url),
                      scrob.album["#text"]
                    )}
                  </>
                )}
                {/* time scrobbled (rendered as a new row) */}
                <span className="stacked-track-info__time-scrobbled">
                  {scrob["@attr"]?.nowplaying ? (
                    <span className="stacked-track-info__time-scrobbled--now">
                      Scrobbling now
                    </span>
                  ) : (
                    getElapsedTimeSince(scrob.date?.uts!).replace("|", ", ")
                  )}
                </span>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    );
  };

  if (error) return <div />;

  if (loading) {
    return (
      <div className="scrobble-list">
        <div className="header-container">
          <h1 className="title">{title}</h1>
        </div>
        <div className="scrobble-list__table-container">
          {/* display loading animation if we're waiting for 'fetchRecentTracks()' to complete */}
          <span className="loader">
            <BarLoader
              width="50%"
              cssOverride={{
                marginTop: 20,
              }}
            />
          </span>
        </div>
      </div>
    );
  }

  return (
    <div className="scrobble-list">
      <div className="header-container">
        <h1 className="title">{title}</h1>
        <div className="page-jump">
          <input
            type="number"
            inputMode="numeric"
            value={pageInput}
            placeholder="Page #"
            onChange={handlePageInputChange}
            min={1}
            onWheel={(event) => event.currentTarget.blur()} // disables scroll on mousewheel
            onKeyDown={(e) => {
              if (e.key === "Enter") handlePageJump();
            }}
          />
          <button
            style={{ backgroundColor: alertColor ?? undefined }}
            onClick={handlePageJump}
          >
            {alertColor === statusColors.SUCCESS ? (
              <IoIosCheckmark style={{ transform: "scale(2)" }} />
            ) : alertColor === statusColors.ERROR ? (
              <IoIosClose style={{ transform: "scale(2)" }} />
            ) : (
              <FaSearch className="page-jump__search-icon" />
            )}
          </button>
        </div>
      </div>
      <div className="scrobble-list__table-container">
        <table className="scrobble-list__table">
          <tbody>
            {recentTracks?.track.map((scrob, index) => (
              <tr
                key={index}
                className={`scrobble-list__row ${
                  scrob["@attr"]?.nowplaying ? "now-playing" : ""
                }`}
              >
                <td>
                  {/* 'album cover' cell */}
                  {getExternalLink(
                    getLastfmAlbumUrl(scrob.album["#text"], scrob.url),
                    <img
                      src={scrob.image[3]["#text"] || defaultLastfmImages.album}
                      alt={scrob.album["#text"]}
                      className="scrobble-list__album-cover"
                    />
                  )}
                </td>
                {generateExpandedView(scrob)}
                {generateStackedView(scrob)}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      <ListPager
        totalCount={playcount}
        currentPage={currentPage}
        pageSize={limit}
        setCurrentPage={setCurrentPage}
        siblingCount={2}
      />
    </div>
  );
};

export default ScrobbleList;
