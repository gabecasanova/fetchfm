import { useState, useEffect, ReactNode } from "react";
import { Button, FormControl } from "react-bootstrap";
import { useNavigate, useLocation } from "react-router-dom";
import { FiLogIn } from "react-icons/fi";
import { BiMessageError } from "react-icons/bi";
import lastfmGet from "api/lastfm/lastfmApi";
import useScreenWidth from "hooks/useScreenWidth";
import AudioWave from "./components/AudioWave";
import "./Login.css";

const Login = () => {
  const navigate = useNavigate();
  const location = useLocation();

  useEffect(() => {
    if (location.pathname !== "/") {
      navigate("/", { replace: true });
    }
  }, [location, navigate]);

  let screenWidth = useScreenWidth();

  // FormControl stuff
  const DEFAULT_MSG = "Your Last.fm Username";
  const [placeholder, setPlaceholder] = useState(DEFAULT_MSG);
  const [username, setUsername] = useState("");
  const [horizontalShake, setHorizontalShake] = useState("");
  const [errorMsg, setErrorMsg] = useState<ReactNode | null>(null);

  // AudioWave stuff
  const [heights, setHeights] = useState<number[]>([]);
  const [loadingWave, setLoadingWave] = useState(true);

  useEffect(() => {
    const newHeights = [5, 2, 7, 6, 3, 9, 5, 3, 2, 8, 7, 4]; // keep length even
    setHeights(newHeights);
    const timer = setTimeout(() => setLoadingWave(false), 200);
    return () => clearTimeout(timer);
  }, []);

  const displayErrorMsg = (msg: ReactNode) => {
    setErrorMsg(msg);
    setHorizontalShake("horizontal-shake");
    setTimeout(() => setHorizontalShake(""), 500);
  };

  const handleSubmit = async () => {
    if (!username) return;

    const userInfo = await lastfmGet({
      method: "user.getInfo",
      user: username,
    });
    if (!userInfo) {
      displayErrorMsg(<>Please enter a valid Last.fm username</>);
      return;
    }

    const recentTrack = await lastfmGet({
      method: "user.getRecentTracks",
      limit: 1,
      user: username,
    });
    if (!recentTrack) {
      displayErrorMsg(
        <>
          Enable "Recent listening" in your
          <a
            href="https://www.last.fm/settings/privacy"
            target="_blank"
            rel="noreferrer"
          >
            Last.fm {screenWidth > 570 ? "privacy" : ""} settings
          </a>
        </>
      );
      return;
    }

    navigate(`/user/${username}`); // Success!!!
  };

  if (loadingWave) return <div />;

  const cn = "login";
  return (
    <div className={cn}>
      <div className={`${cn}__rounded-overlay`} />
      <div className={`${cn}__inner-container`}>
        <h1 className={`${cn}__header`}>Lume.fm</h1>
        <AudioWave
          heights={heights}
          width={7}
          spacingBetween={12}
          colormap="viridis"
          timestep={225} // milliseconds to wait b/w each propagation
        />
        <div className={`${cn}__input-wrapper`}>
          <FormControl
            className={`${cn}__search ${horizontalShake}`}
            spellCheck={false}
            placeholder={placeholder}
            value={username}
            onChange={(e) => setUsername(e.target.value)}
            onFocus={() => {
              setPlaceholder("");
              setErrorMsg(null);
            }}
            onBlur={() => setPlaceholder(DEFAULT_MSG)}
            onKeyDown={(e) => {
              if (e.key === "Enter") handleSubmit();
            }}
            autoCapitalize="none"
          />
          {username.length > 0 && (
            <Button
              className={`${cn}__button ${horizontalShake}`}
              onClick={handleSubmit}
            >
              <FiLogIn
                className={`${cn}__button-icon`}
                color="white"
                size={22}
              />
            </Button>
          )}
        </div>
        {errorMsg && (
          <div className={`${cn}__error-msg`}>
            <BiMessageError
              size={20}
              style={{ marginTop: 4.5, marginRight: 5.5 }}
            />
            {errorMsg}
          </div>
        )}
      </div>
    </div>
  );
};

export default Login;
