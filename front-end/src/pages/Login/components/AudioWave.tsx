import { useState, useEffect } from "react";
import { motion } from "framer-motion";
import createColormap from "colormap";

interface Props {
  heights: number[];
  width: number;
  spacingBetween: number;
  colormap: string;
  timestep: number;
}

const AudioWave = ({
  heights,
  width,
  spacingBetween,
  colormap,
  timestep,
}: Props) => {
  const [animatedHeights, setAnimatedHeights] = useState<number[]>(heights);

  useEffect(() => {
    setAnimatedHeights(heights); // Update animatedHeights when heights prop changes
  }, [heights]);

  // Animate AudioWave segment heights to mimic sound propogation
  useEffect(() => {
    const intervalId = setInterval(() => {
      // Propagate segment heights down the AudioWave
      setAnimatedHeights((prevHeights) => {
        const newAnimatedHeights: number[] = prevHeights.map((_, i) => {
          return prevHeights[(i - 1 + prevHeights.length) % prevHeights.length];
        });
        return newAnimatedHeights;
      });
    }, timestep);
    return () => clearInterval(intervalId);
  }, [timestep]);

  // Generate color gradients given `colormap` format
  const COLORS = createColormap<"hex">({
    colormap: colormap,
    nshades: animatedHeights.length,
  });

  const segmentStyling = (height: number, index: number) => {
    return {
      width,
      height: height * 8,
      backgroundColor: COLORS[index],
      marginRight: spacingBetween,
      borderRadius: width,
    };
  };

  return (
    <div style={{ display: "flex", alignItems: "center" }}>
      {animatedHeights.map((height, index) => (
        <motion.div
          key={index}
          style={segmentStyling(height, index)}
          animate={{
            // `x` handles the necessary padding on either side of the logo
            x: index + (index < animatedHeights.length / 2 ? -140 : 142),
            y: -20,
          }}
          transition={{
            duration: 0,
          }}
        />
      ))}
    </div>
  );
};

export default AudioWave;
