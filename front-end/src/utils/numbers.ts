/**
 * Formats a number or numeric string with commas as thousand separators.
 *
 * @param {string | number} val - The input value to format.
 * @returns {string} The formatted string.
 * @throws {Error} If the input is not a valid number or numeric string.
 *
 * @example
 * // Returns "1,000"
 * formatWithCommas(1000);
 *
 * @example
 * // Returns "123,456,789"
 * formatWithCommas("123456789");
 */
export function formatWithCommas(val: string | number): string {
  if (typeof val === "string") {
    val = Number(val);
  }
  if (typeof val !== "number" || isNaN(val)) {
    throw new Error(
      `Invalid input: "${val}" is not a valid number or numeric string.`
    );
  }
  return val.toLocaleString();
}
