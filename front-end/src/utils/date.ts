/**
 * Converts a Unix timestamp to a mutable Date object.
 * @param unixTime - The Unix timestamp to convert.
 * @returns A mutable Date object.
 */
export function getDateObjFromUnix(unixTime: string): Date {
  return new Date(parseInt(unixTime, 10) * 1_000); // convert to milliseconds
}

/**
 * Converts a Unix timestamp to a human-readable date string.
 * @param unixTime - The Unix timestamp to convert.
 * @returns A date string formatted for the UK locale.
 */
export function getDateStringFromUnix(unixTime: string): string {
  const date = getDateObjFromUnix(unixTime);
  return date.toLocaleDateString("en-UK", {
    day: "2-digit",
    month: "short",
    year: "numeric",
  });
}

/**
 * Converts `year` and `dayOfTheYear` (DOY) to a human-readable date string with weekday included.
 * For example, if year := 2024 and dayOfTheYear := 1, it returns "Mon, 1 Jan 2024".
 * @param year - The year for the date.
 * @param dayOfTheYear - The day of the year (1-based).
 * @returns A date string formatted with the weekday, day, month, and year for the UK locale.
 */
export function getWeekdayDateFromDOY(
  year: number,
  dayOfTheYear: number
): string {
  let formattedDate = new Date(year, 0, dayOfTheYear);
  return formattedDate.toLocaleDateString("en-UK", {
    weekday: "short",
    day: "numeric",
    month: "short",
    year: "numeric",
  });
}

/**
 * Calculates the day-of-the-year (DOY) for the given date.
 * For example, if the date is 1/19/2023, it returns 19.
 * @param date - The date to calculate the day-of-the-year for.
 * @returns The day-of-the-year (1-based).
 */
export function getDOY(d: Date): number {
  return (
    (Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()) -
      Date.UTC(d.getFullYear(), 0, 0)) /
    86_400_000 // number of milliseconds in a day
  );
}

/**
 * Gets the Unix timestamps for the start and end of the day on `year` and `dayOfTheYear`.
 * @param year - The year for the conversion.
 * @param dayOfTheYear - The day of the year to convert (1-based).
 * @returns An object containing `{ from, to }` Unix timestamps.
 */
export function getDailyUnixTimestamps(
  year: number,
  dayOfTheYear: number
): { from: number; to: number } {
  const localDate = new Date(year, 0, dayOfTheYear);
  const from = Math.floor(localDate.getTime() / 1000); // calculate timestamp for start of day

  localDate.setHours(23, 59, 59, 999);
  const to = Math.floor(localDate.getTime() / 1000); // calculate timestamp for end of day

  return { from, to };
}

/**
 * Gets the Unix timestamps for the start and end of the range defined by:
 * @param startYear - The year of the starting day.
 * @param startDOY - The start day's 1-based day of the year.
 * @param endYear - The year of the ending day.
 * @param endDOY - The end day's 1-based day of the year.
 * @returns An object containing `{ from, to }` Unix timestamps.
 */
export function getRangeUnixTimestamps(
  startYear: number,
  startDOY: number,
  endYear: number,
  endDOY: number
): { from: number; to: number } {
  const { from } = getDailyUnixTimestamps(startYear, startDOY);
  const { to } = getDailyUnixTimestamps(endYear, endDOY);
  return { from, to };
}

/**
 * Converts a Unix timestamp into a formatted string displaying the elapsed time since a user scrobbled a track.
 * Returns one of the following outputs:
 * * If within 24 hours, a relative time in minutes or hours
 * * If within the current year, a formatted date and time string (excluding the year)
 * * If from a prior year, a string containing separate `date` and `time` info separated by delimiter "|"
 * @param unixTime - The Unix timestamp to convert.
 * @returns A formatted string representing the elapsed time since the scrobble.
 */
export function getElapsedTimeSince(unixTime: string): string {
  const date = new Date(parseInt(unixTime, 10) * 1000); // Convert to milliseconds
  const now = new Date();
  const diffInSeconds = (now.getTime() - date.getTime()) / 1000;

  const minutes = Math.floor(diffInSeconds / 60);
  const hours = Math.floor(diffInSeconds / 3600);

  if (minutes < 60) {
    return minutes === 1 ? "a minute ago" : `${minutes} minutes ago`;
  } else if (hours < 24) {
    return hours === 1 ? "an hour ago" : `${hours} hours ago`;
  } else if (date.getFullYear() === now.getFullYear()) {
    // the input date is within the current year, we don't need to include year info
    return date
      .toLocaleDateString("en-UK", {
        day: "numeric",
        month: "short",
        hour: "numeric",
        minute: "2-digit",
        hour12: true,
      })
      .replace(" am", "am")
      .replace(" pm", "pm");
  } else {
    // the input date occured in a year prior to the current one, we must explicitly include year info
    const dateString = date.toLocaleDateString("en-UK", {
      day: "numeric",
      month: "short",
      year: "numeric",
    });
    const timeString = date
      .toLocaleTimeString("en-UK", {
        hour: "numeric",
        minute: "2-digit",
        hour12: true,
      })
      .replace(" am", "am")
      .replace(" pm", "pm");
    return `${dateString}|${timeString}`;
  }
}

/**
 * Checks if a given Unix timestamp is in the past relative to the current time.
 * @param {number} unixTime - The timestamp in milliseconds to compare.
 * @returns {boolean} `true` if the given timestamp has already passed, otherwise `false`.
 */
export function isTimestampInPast(unixTime: number): boolean {
  return Date.now() >= unixTime;
}

/**
 * Returns the `dayOfYear` range spanning from the most recent Sunday (i.e., start of the week) to today.
 * @returns An object containing `{ startYear, startDOY, endYear, endDOY }`.
 */
export function getCurrentWeekDOYRange() {
  const now = new Date();
  const currentDayOfWeek = now.getDay(); // 0 (Sunday) to 6 (Saturday)

  // Adjust to the most recent Sunday
  const sunday = new Date(now);
  sunday.setDate(now.getDate() - currentDayOfWeek);

  const startYear = sunday.getFullYear();
  const startDOY = getDOY(sunday);

  const endYear = now.getFullYear();
  const endDOY = getDOY(now);

  return { startYear, startDOY, endYear, endDOY };
}
