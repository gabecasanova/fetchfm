/**
 * Safely parses a JSON string into a typed object.
 *
 * This function attempts to parse a JSON string and returns the parsed object if successful.
 * If parsing fails (e.g., due to invalid JSON), it returns `null`.
 *
 * @template T - The expected type of the parsed object.
 * @param {string | null} value - The JSON string to parse. Can be `null`.
 * @returns {T | null} The parsed object of type `T` if successful, otherwise `null`.
 */
export const parseJSON = <T>(value: string | null): T | null => {
  try {
    return value ? (JSON.parse(value) as T) : null;
  } catch {
    return null;
  }
};
