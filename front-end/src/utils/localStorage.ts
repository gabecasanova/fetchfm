export const LocalStorage = {
  KEYS: {
    SPOTIFY_ACCESS_TOKEN: "spotify_access_token",
  },

  save: (key: string, value: string): void => {
    localStorage.setItem(key, value);
  },

  get: (key: string): string | null => {
    return localStorage.getItem(key);
  },

  remove: (key: string): void => {
    localStorage.removeItem(key);
  },
};
