import { FastAverageColor } from "fast-average-color";

const _FastAverageColor = new FastAverageColor();

/**
 * Extracts the accent color from an image, adjusts its vibrancy, and darkens if too light.
 *
 * @param {string} image - The image URL to extract the color from.
 * @param {number} [vibranceFactor=1.15] - Factor to increase vibrancy (default 1.15).
 * @param {number} [darkenFactor=0.8] - Factor to darken color if luminance is high (default 0.8).
 * @returns {Promise<string>} A promise that resolves to the accent color in rgba format.
 *
 * @see {@link https://github.com/fast-average-color/fast-average-color/}
 */
export async function getAccentColor(
  image: string,
  vibranceFactor = 1.15,
  darkenFactor = 0.8
): Promise<string> {
  const color = await _FastAverageColor.getColorAsync(image, {
    ignoredColor: [
      [255, 255, 255, 255], // white
      [0, 0, 0, 255], // black
    ],
  });

  const rgba = color.rgba;
  let [r, g, b, a] = rgba.match(/\d+/g)!.map(Number);

  // increase vibrancy by scaling the RGB components
  r = Math.min(r * vibranceFactor, 255);
  g = Math.min(g * vibranceFactor, 255);
  b = Math.min(b * vibranceFactor, 255);

  const luminance = 0.2126 * r + 0.7152 * g + 0.0722 * b;

  // darken the color if it's too light ("luminance nearing 255")
  if (luminance > 200) {
    r = Math.max(r * darkenFactor, 0);
    g = Math.max(g * darkenFactor, 0);
    b = Math.max(b * darkenFactor, 0);
  }

  // return the newly calculated accent color
  return `rgba(${r}, ${g}, ${b}, ${a})`;
}
