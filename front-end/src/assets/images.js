import logoSrc from "./media/logo.png";
import defaultProfilePic from "./media/default-profile-pic.png";
import defaultArtistImage from "./media/default-artist-image.png";
import defaultAlbumImage from "./media/default-album-image.png";
import defaultTrackImage from "./media/default-track-image.png";

export const logo = {
  src: logoSrc,
  alt: "Lume.fm logo featuring a crescent moon and music note.",
};

export const defaultLastfmImages = {
  artist: defaultArtistImage,
  album: defaultAlbumImage,
  track: defaultTrackImage,
};

export { defaultProfilePic };
