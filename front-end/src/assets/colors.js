// https://anice.red/

export const statusColors = {
  SUCCESS: "#81c242",
  ERROR: "#ee2b2b",
  WARNING: "#fcbd4f",
};
